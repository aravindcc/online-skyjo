[![License: MIT](https://img.shields.io/github/license/vintasoftware/django-react-boilerplate.svg)](LICENSE.txt)

# Bojo

## About

This was made using the vintasoftware/django-react-boilerplate template.

## Running

### Setup

- Create the migrations for `users` app:  
  `docker-compose run --rm backend python manage.py makemigrations`
- Run the migrations:
  `docker-compose run --rm backend python manage.py migrate`

- Open a command line window and go to the project's directory.
- `docker-compose up -d`
  To access the logs for each service run `docker-compose logs -f service_name` (either backend, frontend, etc)

### Adding packages

- To install a new npm package you can create a bash session inside the running `frontend` container.
- To install a new PyPi package you will have to update the `requirements.in` file and rebuild the `backend` container.