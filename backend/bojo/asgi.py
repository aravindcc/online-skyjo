import os

from django.core.asgi import get_asgi_application
from django.urls import path, re_path

# Fetch Django ASGI application early to ensure AppRegistry is populated
# before importing consumers and AuthMiddlewareStack that may import ORM
# models.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bojo.settings.production")
django_asgi_app = get_asgi_application()

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from exampleapp.consumers import ChatConsumer

application = URLRouter([
    path('ws/game/', AuthMiddlewareStack(ChatConsumer.as_asgi())),
    re_path(r"^.*", django_asgi_app),
])
