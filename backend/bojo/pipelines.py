from social_core.exceptions import AuthForbidden


def enforce_hosted_domain(*, backend, response, **kwargs):
    """
    A pipeline for use with django-social-auth's Google OAuth2 support which
    enforces that the signed in user is a member of one of the
    WHITELISTED_HOSTED_DOMAINS.

    """
    hosted_domain = response.get('hd')
    whitelist = backend.setting('WHITELISTED_HOSTED_DOMAINS', None)
    if whitelist is not None and hosted_domain not in whitelist:
        raise AuthForbidden(backend)


def create_user(strategy, details, backend, user=None, *args, **kwargs):
    if user:
        return {'is_new': False}

    lookup = {
        'ap2138': ('Ar', 'Aravind'),
        "cb964": ('Cl', 'Clara'),
        "fc449": ('Fa', 'Faith'),
        "fb472": ('Fr', 'Freddie'),
        "hc492": ('Ha', 'Harriet'),
        "ehn23": ('Hz', 'Hazel'),
        "la416": ('La', 'Lawrence'),
        "mjh263": ('Ma', 'Maisie'),
        "mtb49": ('Mt', 'Matt')
    }

    looked = lookup[details.get("username")]
    fields = {
        "first_name": looked[1],
        "nickname": looked[0],
        "email": details.get("email"),
    }

    user = strategy.create_user(**fields)
    user.first_name = looked[1]
    user.save()

    return {
        'is_new': True,
        'user': user
    }
