from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        User = get_user_model()
        User.objects.all().delete()
        lookup = {
            'ap2138': ('Ar', 'Aravind'),
            "cb964": ('Cl', 'Clara'),
            "fc449": ('Fa', 'Faith'),
            "fb472": ('Fr', 'Freddie'),
            "hc492": ('Ha', 'Harriet'),
            "ehn23": ('Hz', 'Hazel'),
            "la416": ('La', 'Lawrence'),
            "mjh263": ('Ma', 'Maisie'),
            "mtb49": ('Mt', 'Matt')
        }
        for i, val in enumerate(lookup.values()):
            f = User.objects.create_superuser if i == 0 else User.objects.create_user
            f(first_name=val[1], nickname=val[0], email=f"{val[0]}@skyjo.com")
            print(f"Created account for {val[1]}")
