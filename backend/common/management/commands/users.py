from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.conf import settings
from sesame.utils import get_query_string


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument("firstname",
                            help="firstname of user")
        parser.add_argument("nickname",
                            help="2 letter name of user")

    def handle(self, *args, **kwargs):
        User = get_user_model()
        user = User.objects.create_user(first_name=kwargs['firstname'],
                                        nickname=kwargs['nickname'],
                                        email=f"{kwargs['nickname'].lower()}@skyjo.com",
                                        password='glassonion'
                                        )
        DOMAIN = "http://localhost:8000" if settings.DEBUG else "https://ap2138.user.srcf.net"
        app_url = f"{DOMAIN}/{get_query_string(user)}"
        print(app_url)
