from django.contrib import admin  # noqa
from .models import GameResult, LeaderBoard

admin.site.register(GameResult)
admin.site.register(LeaderBoard)
