import json
from channels.generic.websocket import AsyncWebsocketConsumer
from .skyjo import OUR_GAME, gods, ClientActionType
from datetime import datetime
from random import randint
import asyncio


class ChatConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.player = self.scope['user'].nickname

        # Join room group
        await self.channel_layer.group_add(
            OUR_GAME,
            self.channel_name
        )

        await self.accept()
        await gods.notify_players()
        self.my_task = asyncio.ensure_future(self.run_periodic_task())
        self.my_retry_task = None
        self.sequence = 0

    async def run_periodic_task(self):
        while True:
            await self.send(text_data=json.dumps({"time": True, "stamp": datetime.today().timestamp()}))
            await asyncio.sleep(0.5)

    async def at_least_once_send_routine(self, text_data):
        while True:
            await self.send(text_data=text_data)
            await asyncio.sleep(0.05)

    def at_least_once_send(self, text_data):
        self.sequence += 1
        text_data["sequence"] = self.sequence
        if self.my_retry_task:
            self.my_retry_task.cancel()
        self.my_retry_task = asyncio.ensure_future(
            self.at_least_once_send_routine(text_data=json.dumps(text_data)))

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            OUR_GAME,
            self.channel_name
        )
        await gods.remove_player(self.player)
        self.my_task.cancel()

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        if "stamp" in text_data_json:
            delta = datetime.now() - \
                datetime.fromtimestamp(text_data_json["stamp"])
            gods.add_latency_calc(self.player, delta.total_seconds() / 2)
        else:
            action = text_data_json['action']
            if action == ClientActionType.ACK:
                if self.my_retry_task:
                    self.my_retry_task.cancel()
                    self.my_retry_task = None
                return
            del text_data_json['action']
            text_data_json['self'] = self.player

            # Send message to the gods
            await gods.process_action(self.player, action, text_data_json)

    # Receive message from room group
    async def chat_message(self, event):
        # Send players to WebSocket
        latency = gods.get_latency_for_player(self.player)
        if latency:
            print(f"delaying {self.player}: {latency}")
            await asyncio.sleep(latency)
        out = {**event}
        out['self'] = self.player
        self.at_least_once_send(text_data=out)
