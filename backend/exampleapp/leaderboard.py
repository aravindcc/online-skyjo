from users.models import User
from .models import GameResult, LeaderBoard
import json


def get_leaderboard(players):
    out = LeaderBoard.objects
    for player in players:
        out = out.filter(members__id=player)
    out = out.first()
    if out is None:
        board = LeaderBoard.objects.create()
        board.save()
        board.members.add(*players)
        return board
    return out


def _resp_leaderboard(leaderboard):
    try:
        result = GameResult.objects.filter(
            leadboard=leaderboard).latest('date')
        return {
            "points": json.loads(result.points),
            "previous_leaderboard": json.loads(result.previous_leaderboard),
        }
    except:
        return {}


def get_all_leaderboards(player):
    try:
        board = LeaderBoard.objects.filter(members__id=player).all()
        out = []
        for b in board:
            tran = _resp_leaderboard(b)
            if tran:
                out.append(tran)
        return out
    except:
        return []
