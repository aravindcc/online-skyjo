# Generated by Django 3.1.5 on 2021-01-11 13:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exampleapp', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Choice',
        ),
        migrations.DeleteModel(
            name='Question',
        ),
    ]
