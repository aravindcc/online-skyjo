from __future__ import unicode_literals

import datetime

from django.db import models
from django.utils import timezone
from users.models import User

# Create your models here.


class LeaderBoard(models.Model):
    members = models.ManyToManyField(User, related_name='+')


class GameResult(models.Model):
    points = models.TextField()
    previous_leaderboard = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    leadboard = models.ForeignKey(
        LeaderBoard, blank=True, null=True, on_delete=models.SET_NULL)
