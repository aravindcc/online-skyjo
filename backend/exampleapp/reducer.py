from django.conf import settings
import copy
import random

FOLDED_NUM = -3


class ActionType:
    RESET_GAME = 'RESET_GAME'
    LOCK_DECK = 'LOCK_DECK'
    NEXT_PLAYER = 'NEXT_PLAYER'
    PLAY_GIVE = 'PLAY_GIVE'
    PLAY_TAKE = 'PLAY_TAKE'
    PLAY_REJECT = 'PLAY_REJECT'
    ADD_PLAYER = 'ADD_PLAYER'
    OPEN_GAME = 'OPEN_GAME'


class GoNotDoneException(Exception):
    pass


class RoundFinishException(Exception):
    """
    This error message is thrown when the game is over.
    """
    pass


class DeckLockedException(Exception):
    """
    This error message is thrown if the deck was locked when it was accessed.
    """
    pass


class RowDeletedException(Exception):
    """
    This error message is thrown if the row was removed.
    """
    pass


def _create_initial_state():
    return {
        'players': [],
        'deck': _generate_deck(),
        'play_card': None,
        'prev_play': None,
        'initial_player_ix': None,
        'finish_player_ix': None,
        'current_player_ix': None,
        'deck_locked': False,
        'revealed_count': 0,
        'go_done': False
    }


def _make_board_str():
    return {str(row): {str(col): 0 for col in range(3)} for row in range(4)}


def _make_board(val=0):
    return {row: {col: val for col in range(3)} for row in range(4)}


def _deal_board(deal):
    return {row: {col: deal() for col in range(3)} for row in range(4)}


def _create_player(name, deal):
    return {
        'name': name,
        'cards': _deal_board(deal),
        'mask': _make_board()
    }


def _generate_deck():
    def n(card):
        if card == -2:
            return 5
        if card == 0:
            return 15
        else:
            return 10
    deck = {}
    for c in range(-2, 13):
        deck[c] = n(c)
    deck['_size'] = 150
    return deck


def _remove_players_cards(deck, cards):
    for row in cards.values():
        for card in row.values():
            deck[card] -= 1
            deck['_size'] -= 1


def _give_card(deck, state=None):
    if state is not None and deck['_size'] == 0:
        new_deck = _generate_deck()
        for key in new_deck.keys():
            deck[key] = new_deck[key]
        for player in state['players']:
            _remove_players_cards(deck, player['cards'])
        if state['play_card']:
            deck[state['play_card']] -= 1
            deck['_size'] -= 1

    cards = list(deck.keys())[:15]
    ps = [num / deck['_size'] for num in list(deck.values())[:15]]
    ran = random.uniform(0, 1)
    card = cards[-1]
    for (i, p) in enumerate(ps):
        ran -= p
        if (ran <= 0):
            card = cards[i]
            break
    deck['_size'] -= 1
    deck[card] -= 1
    return card


def _reveal_card(mask, row, col):
    mask[row][col] = 1


def _player_score(player):
    total = 0
    for row in range(4):
        if row not in player['mask'] or row not in player['cards']:
            continue
        for col in range(3):
            total += player['cards'][row][col] * player['mask'][row][col]
    return total


def _get_ranks(leaderboard):
    players = sorted(leaderboard.items(), key=lambda x: x[1])
    points = {player[0]: max(3 - i, 0)
              for (i, player) in enumerate(players)}
    return points


def _get_finished(d):
    return max(list(d.values())) >= 100


def _get_points(state, prev, mapped, current):
    points = {mapped[player['name']]: _player_score(
        player) for player in state['players']}
    points = {}
    min_score = 150
    for player in state['players']:
        score = _player_score(player)
        points[player['name']] = score
        if score < min_score:
            min_score = score

    min_name = []
    for player in state['players']:
        score = _player_score(player)
        if score == min_score:
            min_name.append(player['name'])

    print(f"MIN SCORE PLAYER WAS {min_name}")

    if min_name is not None:
        finisher = state['players'][state['finish_player_ix']]['name']
        print(f"FINISHER WAS {finisher}")
        if len(min_name) > 1 or finisher != min_name[0]:
            points[finisher] = points[finisher] * 2

    for key in points.keys():
        prev[mapped[key]] = points[key] + prev.get(mapped[key], 0)
        current[mapped[key]] = points[key]


def _revealed_all(player):
    revd = 0
    total = 0
    for row in range(4):
        if row not in player['mask'] or row not in player['cards']:
            continue
        for col in range(3):
            revd += player['mask'][row][col]
            total += 1
    return (revd, total == revd)


def _player_maxscore_index(players):
    ix = 0
    max_score = -100
    for i, player in enumerate(players):
        score = _player_score(player)
        if score > max_score:
            max_score = score
            ix = i
    return ix


def _finish_check(state):
    # skip method if other player has already finished the game
    if state['finish_player_ix'] is not None:
        return state

    ix = state['current_player_ix']
    player = state['players'][ix]

    # Mark current user as initiator of last round if all cards
    # have been revealed.
    if _revealed_all(player)[1]:
        state['finish_player_ix'] = ix

    return state


def _drop_filled_rows(cards, mask):
    total = 0
    for row in range(4):
        if row not in cards:
            continue
        start = cards[row][0]
        if start == FOLDED_NUM:
            continue

        can_remove = True
        for col in range(3):
            if mask[row][col] == 0 or cards[row][col] != start:
                can_remove = False
                break
        if can_remove:
            val = cards[row][0]
            del cards[row]
            del mask[row]
            return val
    return None


def _apply_mask(player):
    board = _make_board_str()
    for row in range(4):
        if row not in player['mask'] or row not in player['cards']:
            del board[str(row)]
            continue
        for col in range(3):
            board[str(row)][str(
                col)] = player['cards'][row][col] if player['mask'][row][col] else FOLDED_NUM
    return board


def _send_board_state(state):
    boards = {}
    for player in state['players']:
        boards[player['name']] = _apply_mask(player)
    return {
        'boards': boards,
        'play_card': state['play_card'],
        'deck_locked': state['deck_locked'],
        'prev_play': state['prev_play']
    }


def _get_ix(players, nick):
    for index, item in enumerate(players):
        if item['name'] == nick:
            return index
    return -1


def reducer(state, action):
    if action['type'] == ActionType.RESET_GAME:
        return _create_initial_state()
    elif action['type'] == ActionType.LOCK_DECK:
        return {**state, 'deck_locked': action['lock']}
    elif action['type'] == ActionType.NEXT_PLAYER:
        if not state['go_done']:
            raise GoNotDoneException()
        ix = state['current_player_ix'] + 1
        if ix >= len(state['players']):
            ix = 0
        if state['finish_player_ix'] is not None and state['finish_player_ix'] == ix:
            for i in range(len(state['players'])):
                state['players'][i]['mask'] = _make_board(1)
                _drop_filled_rows(state['players'][i]
                                  ['cards'], state['players'][i]['mask'])
            raise RoundFinishException()
        return {**state, 'current_player_ix': ix, 'go_done': False}
    elif action['type'] == ActionType.PLAY_GIVE:
        if state['deck_locked'] or state['go_done']:
            raise DeckLockedException()
        deck = copy.copy(state['deck'])
        card = _give_card(deck, state)
        return {**state, 'deck': deck, 'play_card': card, 'prev_play': state['play_card'], 'deck_locked': True}
    elif action['type'] == ActionType.PLAY_TAKE:
        if state['go_done']:
            raise DeckLockedException()
        row = int(action['row'])
        col = int(action['col'])

        ix = state['current_player_ix']
        player = state['players'][ix]
        cards = copy.copy(player['cards'])
        mask = copy.copy(player['mask'])

        # update
        old_card = cards[row][col]
        cards[row][col] = state['play_card']
        mask[row][col] = 1

        # remove filled
        filled_card = _drop_filled_rows(cards, mask)
        old_card = old_card if filled_card is None else filled_card

        print(f"{filled_card}")

        players = [*state['players']]
        players[ix]['cards'] = cards
        players[ix]['mask'] = mask

        return _finish_check({
            **state,
            'play_card': old_card,
            'prev_play': state['play_card'],
            'players': players,
            'deck_locked': False,
            'go_done': True
        })
    elif action['type'] == ActionType.PLAY_REJECT:
        if state['go_done']:
            raise DeckLockedException()
        row = int(action['row'])
        col = int(action['col'])
        ix = state['current_player_ix']
        player = state['players'][ix]

        mask = copy.copy(player['mask'])
        mask[row][col] = 1

        filled_card = _drop_filled_rows(player['cards'], mask)
        old_card = state['play_card'] if filled_card is None else filled_card

        print(f"{filled_card}")

        players = [*state['players']]
        players[ix]['mask'] = mask

        return _finish_check({
            **state,
            'play_card': old_card,
            'players': players,
            'deck_locked': False,
            'go_done': True
        })
    elif action['type'] == ActionType.ADD_PLAYER:
        name = action['name']
        players = [*state['players']]
        deck = copy.copy(state['deck'])
        player = _create_player(name, lambda: _give_card(deck))
        players.append(player)
        return {**state, 'players': players, 'deck': deck}
    elif action['type'] == ActionType.OPEN_GAME:
        revealed = state.get('revealed_count', 0)
        players = [*state['players']]
        ix = _get_ix(players, action['self'])
        player = players[ix]

        if (_revealed_all(player)[0] < 2):
            _reveal_card(player['mask'], int(
                action['row']), int(action['col']))
            revealed += 1

        out = {
            **state,
            'players': players,
            'revealed_count': revealed,
        }

        if (revealed == len(players) * 2):
            # start game
            out['initial_player_ix'] = _player_maxscore_index(players)
            out['current_player_ix'] = out['initial_player_ix']
            deck = copy.copy(state['deck'])
            out['play_card'] = _give_card(deck)
            out['deck'] = deck

            # ACCELERATE GAME !
            # if settings.DEBUG:
            #     for i in range(len(out['players'])):
            #         board = _make_board(1)
            #         board[0][0] = 0
            #         out['players'][i]['mask'] = board

        return out

    return state

# deadlock's somewhere
