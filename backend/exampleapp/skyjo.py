import threading
import queue
import random
import asyncio
from users.models import User
from channels.layers import get_channel_layer
from channels.db import database_sync_to_async
from .reducer import reducer, ActionType, _send_board_state, RoundFinishException, GoNotDoneException, DeckLockedException, _get_points, _get_ranks, _get_finished
from .models import GameResult
from .leaderboard import get_leaderboard
import json
from asgiref.sync import async_to_sync

OUR_GAME = 'game'


class Timer:
    def __init__(self, timeout, callback):
        self._timeout = timeout
        self._callback = callback
        self._task = asyncio.ensure_future(self._job())

    async def _job(self):
        await asyncio.sleep(self._timeout)
        await self._callback()

    def cancel(self):
        self._task.cancel()


class ServerActionType:
    TURN_START = 'TURN_START'
    GAME_END = 'GAME_END'


class ClientActionType:
    BEGIN_GAME = 'BEGIN_GAME'
    PLAY_GIVE = 'PLAY_GIVE'
    PLAY_TAKE = 'PLAY_TAKE'
    OPEN_CARD = 'OPEN_CARD'
    PLAY_REJECT = 'PLAY_REJECT'
    ACK = 'ACK'
    ROUND = 'ROUND_ACK'
    RECAST = 'RECAST'
    CLEAR_ACK = 'CLEAR_ACK'


class Skyjo:
    def __init__(self):
        self.lock = threading.RLock()
        self.players = []
        self.beganGame = False
        self.state = {}
        self.leader = None
        self.channel_layer = get_channel_layer()
        self.group_name = OUR_GAME
        self.waiting_ack = None
        self.name_map = {}
        self.ackTimer = None
        self.gameEnded = False
        self.roundEnded = False
        self.round = -1
        self.leaderboard = {}
        self.points = {}
        self.saveToLeaderBoard = []
        self.queue = queue.Queue()
        self.player_latency = {}
        threading.Thread(target=self.worker, daemon=True).start()

    def add_player(self, player):
        with self.lock:
            if not self.beganGame and player.nickname not in self.players:
                if self.leader is None:
                    self.leader = player.nickname
                self.saveToLeaderBoard.append(player.pk)
                self.players.append(player.nickname)
                self.player_latency[player.nickname] = 0.2
                self.name_map[player.nickname] = player.first_name
                return True
            return False

    async def remove_player(self, player):
        with self.lock:
            if self.beganGame:
                return

            try:
                self.players.remove(player)
                self.saveToLeaderBoard.remove(player)
            except Exception as e:
                print(str(e))
                pass

            print(f"REMOVING {len(self.players)}")

            if len(self.players) > 0:
                if self.leader == player:
                    self.leader = self.players[0]
                await self.notify_players()
            else:
                self.beganGame = False
                self.leader = None

    async def notify_players(self):
        with self.lock:
            await self.multicast_state()

    async def multi_turn_start(self):
        with self.lock:
            cp = self.state['players'][self.state['current_player_ix']]['name']
            await self.send_message(ServerActionType.TURN_START, {'player': cp, 'name': self.name_map[cp]})

    def get_current_leaderboard(self):
        with self.lock:
            return (self.leaderboard, self.points)

    def add_latency_calc(self, player, latency):
        if player in self.player_latency:
            self.player_latency[player] *= 0.125
            self.player_latency[player] += 0.875 * latency

    def get_latency_for_player(self, player):
        if self.player_latency and player in self.player_latency:
            return max(self.player_latency.values()) - self.player_latency[player]
        return None

    async def go_next_player(self):
        with self.lock:
            try:
                self.state = reducer(
                    self.state, {'type': ActionType.NEXT_PLAYER})
                print("GONE NEXT PLAYER")
                await self.multi_turn_start()
            except RoundFinishException:
                self.roundEnded = True
                if self.calc_next_round():
                    self.waiting_ack = [*self.players]
                    await self.multicast_state()
                else:
                    print("GAME ENDED!!!!")
                    try:
                        self.state = reducer(
                            self.state, {'type': ActionType.PLAY_GIVE})
                        self.state = reducer(
                            self.state, {'type': ActionType.LOCK_DECK, 'lock': True})
                    except:
                        pass
                    self.gameEnded = True
                    self.round += 1
                    data = self._get_all_state()
                    await self.send_message(ServerActionType.GAME_END, data)
                    await database_sync_to_async(self.save_result)(self.leaderboard)
                    self.reset_self()
                    print("RESET SELF")
            except GoNotDoneException:
                await self.multi_turn_start()

    def worker(self):
        while True:
            item = self.queue.get()
            async_to_sync(self._process_action)(*item)
            self.queue.task_done()

    async def process_action(self, *args):
        self.queue.put(args)

    async def _process_action(self, player, action, data):
        with self.lock:
            if self.leader is None:
                return

            if action == ClientActionType.RECAST:
                await self.multicast_state()
                return

            if action == ClientActionType.CLEAR_ACK:
                await self.clear_ack()
                return

            if self.waiting_ack:
                # if action == ClientActionType.ACK and not self.roundEnded:
                #     self.waiting_ack = [
                #         x for x in self.waiting_ack if x != player]
                #     if len(self.waiting_ack) == 0:
                #         if self.ackTimer:
                #             self.ackTimer.cancel()
                #             self.ackTimer = None
                #         self.waiting_ack = None
                #         await self.go_next_player()
                if action == ClientActionType.ROUND and self.roundEnded:
                    self.waiting_ack = [
                        x for x in self.waiting_ack if x != player]
                    if len(self.waiting_ack) == 0:
                        self.waiting_ack = None
                        self.start_next_round()
                        await self.multicast_state()
                return

            if action == ClientActionType.ACK or action == ClientActionType.ROUND:
                return

            if action == ClientActionType.BEGIN_GAME:
                if player != self.leader or self.beganGame:
                    return
                self.start_game()
                await self.multicast_state()
            elif action == ClientActionType.OPEN_CARD and self.state['revealed_count'] != len(self.players) * 2:
                self.state = reducer(
                    self.state, {'type': ActionType.OPEN_GAME, **data})
                if self.state['initial_player_ix'] != None:
                    cp = self.state['players'][self.state['initial_player_ix']]['name']
                    data = {'player': cp, 'name': self.name_map[cp]}
                    await self.send_message(ServerActionType.TURN_START, data)
                else:
                    await self.multicast_state()
                return

            # get current player
            try:
                if self.state['players'][self.state['current_player_ix']]['name'] != player:
                    return
            except:
                return

            try:
                if action == ClientActionType.PLAY_GIVE:
                    self.state = reducer(
                        self.state, {'type': ActionType.PLAY_GIVE, **data})
                    self.state = reducer(
                        self.state, {'type': ActionType.LOCK_DECK, 'lock': True})
                    await self.multicast_state()
                elif action == ClientActionType.PLAY_TAKE:
                    locked_deck = self.state['deck_locked']
                    last_play = self.state['prev_play']
                    self.state = reducer(
                        self.state, {'type': ActionType.PLAY_TAKE, **data})
                    self.queue.put(["", ClientActionType.CLEAR_ACK, {}])
                    await self.multicast_state(override_data={
                        'transistion': {
                            'row': data['row'],
                            'col': data['col'],
                            'deck_locked': locked_deck,
                            'play_card': self.state['play_card'],
                            'prev_play': self.state['prev_play'],
                            'last_play': last_play
                        }
                    })
                elif action == ClientActionType.OPEN_CARD and self.state['revealed_count'] == len(self.players) * 2 and self.state['deck_locked'] == True:
                    print("HAS ASKED TO OPEN CARD!!!")
                    self.state = reducer(
                        self.state, {'type': ActionType.PLAY_REJECT, **data})
                    await self.go_next_player()
            except DeckLockedException:
                pass

    def _get_all_state(self, override_data=None):
        with self.lock:
            extra = override_data if override_data else (
                _send_board_state(self.state) if self.beganGame else {})
            try:
                player = self.state['players'][self.state['current_player_ix']]['name']
            except:
                player = None
            return {
                'type': 'chat_message',
                'players': self.players,
                'leader': self.leader,
                'began': self.beganGame,
                'player': player,
                'round': self.round,
                'roundEnded': self.roundEnded,
                'ended': self.gameEnded,
                **extra
            }

    async def clear_ack(self):
        with self.lock:
            await asyncio.sleep(0.4)
            self.waiting_ack = None
            await self.go_next_player()

    async def multicast_state(self, override_data=None):
        with self.lock:
            # if lock_step:
            #     self.waiting_ack = [*self.players]
            #     self.ackTimer = Timer(2.0, self.clear_ack)
            await self.channel_layer.group_send(
                OUR_GAME,
                self._get_all_state(override_data)
            )

    def start_game(self):
        with self.lock:
            self.beganGame = True
            self.round = 0
            self.leaderboard = {}
            self.points = {}
            print("CLEARING LEADERBOARD???")
            # create deck
            self.state = reducer(self.state, {'type': ActionType.RESET_GAME})
            # permute players
            random.shuffle(self.players)
            # add players
            for player in self.players:
                self.leaderboard[self.name_map[player]] = 0
                self.points[self.name_map[player]] = 0
                self.state = reducer(
                    self.state, {'type': ActionType.ADD_PLAYER, 'name': player})

    async def send_message(self, action, data={}):
        with self.lock:
            if action == ServerActionType.TURN_START:
                await self.channel_layer.group_send(
                    OUR_GAME,
                    {
                        'type': 'chat_message',
                        'ack': True,
                        **data,
                        'data': self._get_all_state(),
                    }
                )
            elif action == ServerActionType.GAME_END:
                await self.channel_layer.group_send(
                    OUR_GAME,
                    {
                        'type': 'chat_message',
                        'game_end': True,
                        'data': data if data else self._get_all_state(),
                    }
                )

    def calc_next_round(self):
        with self.lock:
            _get_points(self.state, self.leaderboard,
                        self.name_map, self.points)
            return not _get_finished(self.leaderboard)

    def start_next_round(self):
        with self.lock:
            self.round += 1
            self.roundEnded = False
            self.state = reducer(self.state, {'type': ActionType.RESET_GAME})
            for player in self.players:
                self.state = reducer(
                    self.state, {'type': ActionType.ADD_PLAYER, 'name': player})

    def save_result(self, leaderboard):
        points = _get_ranks(leaderboard)
        saveToLeaderBoard = get_leaderboard(self.saveToLeaderBoard)
        try:
            previous = GameResult.objects.filter(
                leadboard=saveToLeaderBoard).latest('date')
            leaderboard = json.loads(previous.previous_leaderboard)
            previous_points = json.loads(previous.points)
            all_keys = set(list(leaderboard.keys()) + list(points.keys()))
            for key in all_keys:
                leaderboard[key] = previous_points.get(
                    key, 0) + leaderboard.get(key, 0)
            GameResult.objects.create(points=json.dumps(
                points), previous_leaderboard=json.dumps(leaderboard), leadboard=saveToLeaderBoard)
        except Exception as e:
            print(f"FAILED ADDING {e}")
            out = {}
            for user in saveToLeaderBoard.members.all():
                out[user.first_name] = 0
            GameResult.objects.create(points=json.dumps(
                points), previous_leaderboard=json.dumps(out), leadboard=saveToLeaderBoard)

    def ask_to_leave(self, user):
        with self.lock:
            if user.nickname not in self.players or not self.beganGame:
                return False

            self.players.remove(user.nickname)
            if self.waiting_ack and user.nickname in self.waiting_ack:
                self.waiting_ack.remove(user.nickname)
                if len(self.waiting_ack) == 0:
                    self.waiting_ack = None
                    self.start_next_round()

            if self.leaderboard and user.nickname in self.leaderboard:
                self.leaderboard.remove(user.nickname)
                self.points.remove(user.nickname)

            leavingPlayer = None
            atI = None
            for i, player in enumerate(self.state["players"]):
                if player['name'] == user.nickname:
                    leavingPlayer = player
                    atI = i
                    break

            if leavingPlayer is None:
                return False

            # deal cards back in
            for row in leavingPlayer['cards'].values():
                for card in row.values():
                    self.state['deck'][card] += 1

            if self.state["current_player_ix"] is not None and self.state["current_player_ix"] > atI:
                self.state["current_player_ix"] -= 1

            if self.state["finish_player_ix"] is not None and self.state["finish_player_ix"] > atI:
                self.state["finish_player_ix"] -= 1

            del self.state["players"][atI]

            if len(self.state["players"]) == 0:
                self.reset_self()
            else:
                self.queue.put(["", ClientActionType.RECAST, {}])

    def reset_self(self):
        with self.lock:
            # reset the gods
            self.players = []
            self.beganGame = False
            self.state = {}
            self.leader = None
            self.group_name = OUR_GAME
            self.waiting_ack = None
            self.name_map = {}
            self.player_latency = {}
            self.ackTimer = None
            self.gameEnded = False
            self.roundEnded = False
            self.round = -1
            self.saveToLeaderBoard = []


gods = Skyjo()
