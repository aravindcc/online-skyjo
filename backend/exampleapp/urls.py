from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

should_exempt_csrf = csrf_exempt if settings.DEBUG else (lambda a: a)

from .views import LeaderView, index, LeaderboardView, ScoreView, ClearGameAndSaveView, ClearGameView, LeaveView, StateView

app_name = 'exampleapp'
urlpatterns = [
    path('leader/', should_exempt_csrf(LeaderView.as_view()), name="leader_view"),
    path('state/', should_exempt_csrf(StateView.as_view()), name="state_view"),
    path('board/', should_exempt_csrf(LeaderboardView.as_view()), name="board_view"),
    path('score/', should_exempt_csrf(ScoreView.as_view()), name="score_view"),
    path('clear/', should_exempt_csrf(ClearGameView.as_view()), name="clear_view"),
    path('save/', should_exempt_csrf(ClearGameAndSaveView.as_view()),
         name="clear_save_view"),
    path('leave/', should_exempt_csrf(LeaveView.as_view()), name="leave_view"),
    path('' if settings.DEBUG else 'index.html', index, name='index'),
]
