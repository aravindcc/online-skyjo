from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAdminUser
from django.shortcuts import render
from django.conf import settings
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.decorators import login_required

from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import PermissionDenied
from sesame.utils import get_query_string
from .skyjo import gods
from .leaderboard import get_all_leaderboards
from users.models import User
import asyncio
import json


# Create your views here.
@login_required()
def index(request):
    return render(request, 'exampleapp/index.html', context={})


class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class DebugabbleView(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication) if settings.DEBUG else (
        SessionAuthentication, BasicAuthentication)


class ClearGameView(DebugabbleView):
    permission_classes = (IsAdminUser,)

    def get(self, request):
        with gods.lock:
            gods.reset_self()
            gods.notify_players()
            return Response("OK!")


class ClearGameAndSaveView(DebugabbleView):
    permission_classes = (IsAdminUser,)

    def get(self, request):
        with gods.lock:
            if gods.round < 2:
                return Response(f"Come on - more rounds PLZ on round {gods.round}")
            gods.save_result(gods.leaderboard)
            gods.reset_self()
            gods.notify_players()
            return Response("OK!")


class LeaderboardView(DebugabbleView):

    def get(self, request, format='json'):
        return Response({
            "boards": get_all_leaderboards(request.user.pk)
        })


class StateView(DebugabbleView):
    permission_classes = (IsAdminUser,)

    def get(self, request, format='json'):
        with gods.lock:
            return Response(gods.state)


class ScoreView(DebugabbleView):

    def get(self, request, format='json'):
        (leaderboard, points) = gods.get_current_leaderboard()
        return Response({
            "points": points,
            "previous_leaderboard": leaderboard,
        })


class LeaderView(DebugabbleView):

    """
    post:
    Attempt to login email.
    get:
    Get auth status.
    """

    def post(self, request, format='json'):
        if gods.add_player(request.user):
            return Response('OK!')
        return Response("failed")

    def get(self, request):
        with gods.lock:
            if gods.leader is None:
                return Response("failed")
            gods.add_player(request.user)
            return Response('OK!')


class LeaveView(DebugabbleView):

    """
    post:
    Attempt to login email.
    get:
    Get auth status.
    """

    def post(self, request, format='json'):
        if gods.ask_to_leave(request.user):
            return Response('OK!')
        return Response("failed")
