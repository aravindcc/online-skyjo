import React from 'react';
import { hot } from 'react-hot-loader/root';
import { QueryClient, QueryClientProvider } from 'react-query';

import 'antd/dist/antd.css';
import Boot from './pages/Boot';

const queryClient = new QueryClient();
const App = () => {
    return (
        <>
            <QueryClientProvider client={queryClient}>
                <Boot />
            </QueryClientProvider>
        </>
    );
};

export default hot(App);
