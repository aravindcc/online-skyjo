import React from 'react'
import { Alert } from 'antd'

export default ({ style = {}, message, type }) => (
  <div
    style={{
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      ...style
    }}
  >
    <Alert message={message} type={type} showIcon />
  </div>
)
