import React, { useCallback, useEffect, useState } from 'react';
import { animate } from 'framer-motion';
import { FOLDED_NUM } from '../constants';
import JoCard from './JoCard';

export const AnimatedCard = ({
    startPoint,
    endPoint,
    value,
    setOpenPileVal,
    count,
    reverseObj,
    onCompleteExtra,
    CARD_HEIGHT,
    CARD_WIDTH,
}) => {
    const [x, setX] = useState(0);
    const [y, setY] = useState(0);
    const [a, setA] = useState(1);

    const [display, setDisplay] = useState('none');
    const [showingValue, setShowingValue] = useState(value);

    const showing = showingValue != FOLDED_NUM;

    const onAnimationComplete = useCallback(() => {
        if (reverseObj) {
            setDisplay(undefined);
            animate(reverseObj.startPoint.x, reverseObj.endPoint.x, {
                onUpdate: setX,
            });
            const onPoint = reverseObj.endPoint.y == reverseObj.startPoint.y;
            const starty = onPoint ? reverseObj.startPoint.y + 25 : reverseObj.startPoint.y;
            animate(starty, reverseObj.endPoint.y, {
                onComplete: (() => {
                    setOpenPileVal(reverseObj.value);
                    reverseObj.onAnimationComplete();
                    setDisplay('none');
                }).bind(reverseObj),
                onUpdate: setY,
            });
            if (onPoint) {
                animate(0, 1, { onUpdate: setA });
            } else {
                setA(1);
            }
            setShowingValue(reverseObj.value);
        } else {
            setOpenPileVal(value);
            setDisplay('none');
            onCompleteExtra && onCompleteExtra();
        }
    }, [setDisplay, setOpenPileVal, value, reverseObj, onCompleteExtra]);

    useEffect(() => {
        if (startPoint == undefined) return;
        setDisplay(undefined);
        animate(startPoint.x, endPoint.x, { onUpdate: setX });
        const onPoint = endPoint.y == startPoint.y;
        const endy = onPoint ? startPoint.y + 25 : endPoint.y;
        animate(startPoint.y, endy, { onUpdate: setY, onComplete: onAnimationComplete });
        if (onPoint) {
            animate(1, 0, { onUpdate: setA });
        } else {
            setA(1);
        }

        setShowingValue(value);
    }, [startPoint, endPoint, count]);

    return (
        <JoCard
            style={{
                display,
                width: CARD_WIDTH,
                height: CARD_HEIGHT,
                position: 'absolute',
                left: x,
                top: y,
                opacity: a,
            }}
            value={showingValue}
        >
            {showing && <h1>{showingValue}</h1>}
        </JoCard>
    );
};
