import { Row } from 'antd';
import React from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import { BoardCard } from './BoardCard';

export const AnimatedRow = ({
    i,
    row,
    checkVisible,
    updateCell,
    doReverseAnim,
    lookupPos,
    CARD_WIDTH,
    CARD_HEIGHT,
}) => {
    return (
        <AnimatePresence>
            {checkVisible(row, i) && (
                <motion.div
                    initial={{ flexGrow: 1 }}
                    animate={{ flexGrow: 1 }}
                    exit={{ height: 0, opacity: 0, flexGrow: 0 }}
                    transition={{
                        delay: 0.85,
                        x: { type: 'spring', stiffness: 100 },
                        default: { duration: 0.5 },
                    }}
                >
                    <Row style={{ height: CARD_HEIGHT }}>
                        {Object.entries(row).map(([j, num]) => (
                            <BoardCard
                                key={`${i} ${j}`}
                                i={i}
                                j={j}
                                val={num}
                                updateCell={updateCell}
                                doReverseAnim={doReverseAnim}
                                lookupPos={lookupPos}
                                CARD_WIDTH={CARD_WIDTH}
                                CARD_HEIGHT={CARD_HEIGHT}
                            />
                        ))}
                    </Row>
                </motion.div>
            )}
        </AnimatePresence>
    );
};
