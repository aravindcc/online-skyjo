import React, { useCallback, useEffect, useRef } from 'react';
import { DropZones, FOLDED_NUM } from '../constants';
import { onResize } from './BoardCard';
import { DragCard } from './DragCard';

export const Bins = ({ openPileVal, deckVal, drawDeck, lookupPos, CARD_WIDTH, CARD_HEIGHT }) => {
    const openPile = useRef();
    const deck = useRef();

    const saveLookup = useCallback(() => {
        if (openPile.current && deck.current) {
            lookupPos[DropZones.FACEUP] = openPile.current.getBoundingClientRect();
            lookupPos[DropZones.FACEDOWN] = deck.current.getBoundingClientRect();
        }
    }, [lookupPos]);

    useEffect(saveLookup, [openPile.current, deck.current]);
    onResize(saveLookup);

    return (
        <div className="JoTopBins" style={{ height: CARD_HEIGHT, marginBottom: 20 }}>
            <DragCard
                key={0}
                dragID={DropZones.FACEUP}
                value={openPileVal}
                overOpacity={deckVal != FOLDED_NUM ? 0.2 : undefined}
                ref={openPile}
                CARD_WIDTH={CARD_WIDTH}
            />
            <DragCard
                key={1}
                dragID={DropZones.FACEDOWN}
                value={deckVal}
                handleClick={drawDeck}
                ref={deck}
                CARD_WIDTH={CARD_WIDTH}
            />
        </div>
    );
};
