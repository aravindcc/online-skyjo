import { Col } from 'antd';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useDrop } from 'react-dnd';
import CardFlip from '../components/cardFlip';
import { DropZones, FOLDED_NUM } from '../constants';
import JoCard from './JoCard';

export function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

export function onResize(f) {
    useEffect(() => {
        // Add event listener
        window.addEventListener('resize', f);

        // Remove event listener on cleanup
        return () => window.removeEventListener('resize', f);
    }, []); // Empty array ensures that effect is only run on mount
}

export const BoardCard = ({
    val,
    i,
    j,
    updateCell,
    doReverseAnim,
    lookupPos,
    CARD_HEIGHT,
    CARD_WIDTH,
}) => {
    const hasValue = val != FOLDED_NUM;
    const [isFlipped, setIsFlipped] = useState(false);
    const prevValue = usePrevious(hasValue);
    const container = useRef();

    const saveLookup = useCallback(() => {
        if (container.current) {
            lookupPos[i][j] = { ...container.current.getBoundingClientRect() };
        }
    }, [i, j, lookupPos, container]);

    useEffect(saveLookup, [i, j, container.current]);
    onResize(saveLookup);

    useEffect(() => {
        if (!prevValue && hasValue) {
            setIsFlipped(true);
        } else if (prevValue && !hasValue) {
            setIsFlipped(false);
        }
    }, [hasValue]);

    const handleClick = useCallback(
        () => !hasValue && updateCell(undefined, i, j, DropZones.TOUCH),
        [hasValue, updateCell]
    );

    const [{ isOver }, drop] = useDrop({
        accept: [DropZones.FACEUP, DropZones.FACEDOWN],
        drop: (item, monitor) => {
            doReverseAnim(monitor, val);
            updateCell(item.v, i, j, item.type);
        },
        collect: (monitor) => ({
            isOver: !!monitor.isOver(),
        }),
    });

    return (
        <Col span={8}>
            <div className="joCardContainer" ref={drop}>
                <div style={{ width: CARD_WIDTH, height: CARD_HEIGHT }}>
                    <CardFlip
                        isFlipped={isFlipped}
                        flipSpeedBackToFront={1.4}
                        flipSpeedFrontToBack={1.4}
                        ref={container}
                    >
                        <JoCard
                            key={0}
                            onClick={handleClick}
                            style={{
                                opacity: isOver ? 0.2 : 1,
                            }}
                            value={FOLDED_NUM}
                        />
                        <JoCard
                            key={1}
                            style={{
                                opacity: isOver ? 0.2 : 1,
                            }}
                            value={val}
                        >
                            <h1>{val}</h1>
                        </JoCard>
                    </CardFlip>
                </div>
            </div>
        </Col>
    );
};
