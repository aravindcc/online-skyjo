import React from 'react';
import { useDrag } from 'react-dnd';
import { FOLDED_NUM } from '../constants';
import CardFlip from './cardFlip';
import JoCard from './JoCard';

export const DragCard = React.forwardRef(
    ({ value, dragID, handleClick, overOpacity, CARD_WIDTH }, ref) => {
        const [{ opacity }, dragRef] = useDrag({
            item: { type: dragID, v: value },
            collect: (monitor) => ({
                opacity: monitor.isDragging() ? 0.5 : 1,
            }),
        });
        const style = { width: CARD_WIDTH };
        const showing = value != FOLDED_NUM;
        return (
            <CardFlip
                isFlipped={showing}
                flipSpeedBackToFront={1.4}
                flipSpeedFrontToBack={1.4}
                shouldAnimate={() => showing}
                ref={ref}
            >
                <JoCard
                    key={0}
                    onClick={handleClick}
                    style={{
                        ...style,
                        opacity,
                    }}
                    value={FOLDED_NUM}
                />
                <JoCard
                    key={1}
                    fRef={showing && !overOpacity ? dragRef : undefined}
                    style={{
                        ...style,
                        opacity: overOpacity || opacity,
                    }}
                    value={value}
                >
                    {showing && <h1>{value}</h1>}
                </JoCard>
            </CardFlip>
        );
    }
);
