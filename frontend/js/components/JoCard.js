import React from 'react';
import { getColor } from '../constants';

export default ({ style, children, value, fRef, onClick }) => {
    return (
        <div
            ref={fRef}
            className="joCard"
            style={{
                ...style,
            }}
            onClick={onClick}
        >
            <img src={getColor(value)} />
        </div>
    );
};
