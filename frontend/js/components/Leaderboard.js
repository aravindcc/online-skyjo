import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useQuery } from 'react-query';
import Loading from './Loader';
import CentralAlert from './Alert';
import {
    PoweroffOutlined,
    ReloadOutlined,
    MinusOutlined,
    QuestionCircleOutlined,
    LogoutOutlined,
} from '@ant-design/icons';
import { Button, Empty, message, Pagination, Table, Tag, Popconfirm } from 'antd';
import { onResize } from './BoardCard';
import { postData } from '../data';

export const DeleteButton = ({
    removeOption,
    id,
    confirms,
    message,
    icon,
    type,
    onClick,
    size,
    children,
}) => {
    let props = {
        type: type || 'danger',
        size: size || 'small',
        icon: icon || <MinusOutlined />,
    };
    const next = onClick || (() => removeOption(id));
    if (confirms) {
        return (
            <Popconfirm
                title={message || 'Are you sure？'}
                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                onConfirm={next}
            >
                {<Button {...props}>{children}</Button>}
            </Popconfirm>
        );
    }
    return (
        <Button {...props} onClick={next}>
            {children}
        </Button>
    );
};

const requestLeave = () => {
    postData('/leave/', {}).then(() => {
        message.info({
            content: 'Bye!',
            style: { textAlign: 'left', marginLeft: 30 },
        });
    });
};

const LeaderTable = ({ result, marginTable, tableHeight, tableWidth, absorb }) => {
    if (!result) {
        return (
            <div
                style={{
                    width: tableWidth || '70%',
                    height: tableHeight,
                    marginTop: 50,
                }}
            >
                <Empty />
            </div>
        );
    }

    const { previous_leaderboard: data, points: inpPoints } = result;
    const points = inpPoints || {};
    var dataSource = [];
    var i = 0;
    for (const key in data) {
        const p = points[key] || 0;
        dataSource.push({
            key: i,
            name: key,
            score: data[key] + (absorb ? p : 0),
            point: p,
        });
        i++;
    }
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Score',
            dataIndex: 'score',
            key: 'score',
            defaultSortOrder: 'descend',
            sorter: (a, b) => a.score + a.point - (b.score + b.point),
            render: (score, a) => {
                let color = 'black';
                if (a.point == 3) color = 'darkgreen';
                if (a.point == 2) color = 'darkseagreen';
                if (a.point == 1) color = 'green';
                return (
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            fontSize: '1.4rem',
                            fontWeight: 'bold',
                        }}
                    >
                        {score}
                        {a.point != 0 && (
                            <Tag style={{ marginLeft: 10 }} color={color}>
                                + {a.point}
                            </Tag>
                        )}
                    </div>
                );
            },
        },
    ];
    return (
        <Table
            style={{
                width: tableWidth || '70%',
                height: tableHeight,
                margin: marginTable == undefined ? 20 : marginTable,
            }}
            dataSource={dataSource}
            columns={columns}
            pagination={false}
            scroll={{ y: tableHeight - 55 }}
        />
    );
};

export default ({ requestLeader, inputLocation, round, marginTable, tableWidth }) => {
    const location = inputLocation || 'board';
    const { isLoading, error, data: result, refetch } = useQuery(location, () =>
        fetch(`/${location}/`).then((res) => {
            if (res.status == 200) {
                return res.json();
            } else {
                throw new Error('Invalid Request!');
            }
        })
    );

    const [tableHeight, setTableHeight] = useState(0);
    const [selectedLeaderboard, setSelctedLeaderboard] = useState(1);

    const resizeTable = useCallback(() => {
        const height = document.documentElement.clientHeight;
        const h = height * 0.9 - 155;
        setTableHeight(h);
    }, [setTableHeight]);

    onResize(resizeTable);
    useEffect(resizeTable, []);
    useEffect(() => {
        refetch();
    }, [round]);

    if (isLoading) return <Loading style={{ height: '100%' }} fontSize="3rem" />;

    if (error)
        return (
            <CentralAlert
                style={{ height: '100%' }}
                message="Hmm can't load leaderboard"
                type="error"
            />
        );

    const table = (result['boards'] && result['boards'][selectedLeaderboard - 1]) || result;
    const pageLength = (result['boards'] || []).length;
    return (
        <div
            style={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
        >
            <LeaderTable
                {...{
                    result: table,
                    marginTable,
                    tableHeight,
                    tableWidth,
                    absorb: !!requestLeader,
                }}
            />
            {!requestLeader && (
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-around',
                        width: '90%',
                        marginTop: 20,
                        marginBottom: 20,
                        flexShrink: 0,
                    }}
                >
                    <Button type="primary" icon={<ReloadOutlined />} size="large" onClick={refetch}>
                        Refresh
                    </Button>
                    <DeleteButton
                        type="ghost"
                        icon={<LogoutOutlined />}
                        size="large"
                        onClick={requestLeave}
                        confirms
                    >
                        Leave
                    </DeleteButton>
                </div>
            )}
            {requestLeader && (
                <>
                    <Pagination
                        disabled={pageLength == 0}
                        current={selectedLeaderboard}
                        onChange={setSelctedLeaderboard}
                        total={pageLength}
                        defaultPageSize={1}
                    />
                    <Button
                        type="primary"
                        icon={<PoweroffOutlined />}
                        size="large"
                        style={{
                            marginTop: '40px',
                            flexShrink: 0,
                        }}
                        onClick={requestLeader}
                    >
                        Start Game!
                    </Button>
                </>
            )}
        </div>
    );
};
