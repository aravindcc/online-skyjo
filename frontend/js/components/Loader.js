import React from 'react'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'

export default ({ style = {}, fontSize = 24 }) => (
  <div
    style={{
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      ...style
    }}
  >
    {' '}
    <Spin indicator={<LoadingOutlined style={{ fontSize }} spin />} />
  </div>
)
