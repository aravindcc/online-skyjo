import React from 'react';
import { motion } from 'framer-motion';
import { ProfileOutlined } from '@ant-design/icons';
import { Col, Row } from 'antd';
import { ogData } from '../data';
import { FOLDED_NUM, getRawColor } from '../constants';

const notSame = (row) => row[0] == FOLDED_NUM || !row.every((v) => v === row[0]);
const MiniGrid = ({ board: inputBoard, index, selected, value, self, onClick, playing, began }) => {
    const board = inputBoard || ogData();

    if (!began) {
        return <h1 className="TurnMenuItem">{self ? 'me' : value}</h1>;
    }

    return (
        <div
            style={{
                flexGrow: 1,
                flexShrink: 0,
                minWidth: 50,
                maxWidth: 100,
                margin: 5,
                position: 'relative',
                overflow: 'hidden',
            }}
            onClick={onClick}
        >
            {!(selected || self) && (
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'stretch',
                        width: '100%',
                        height: '100%',
                        position: 'absolute',
                    }}
                >
                    {Object.values(board).map((row, i) => {
                        const items = Object.values(row);
                        return (
                            notSame(items) && (
                                <Row key={i} style={{ flexGrow: 1 }}>
                                    {items.map((col, j) => {
                                        return (
                                            <Col
                                                key={j}
                                                span={8}
                                                style={{
                                                    backgroundColor: getRawColor(col),
                                                    border: '1px solid black',
                                                }}
                                            />
                                        );
                                    })}
                                </Row>
                            )
                        );
                    })}
                </div>
            )}
            {(selected || self) && (
                <div
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: '100%',
                        height: '100%',
                        color: 'black',
                        position: 'absolute',
                        boxShadow: `inset 0 0 0px ${playing ? 10 : 5}px black`,
                    }}
                >
                    <h1 style={{ margin: 0 }}>{self ? 'me' : value}</h1>
                </div>
            )}
            {playing && (
                <div
                    style={{
                        width: '100%',
                        height: '100%',
                        position: 'absolute',
                        boxShadow: `inset 0 0 0px 5px red`,
                    }}
                />
            )}
        </div>
    );
};

export const TurnBar = ({
    data: people,
    selected,
    onClick,
    playing,
    self,
    showLeaderBoard,
    boards,
    began,
}) => {
    return (
        <motion.div
            className="TurnBar"
            initial={{ bottom: 0, opacity: 1 }}
            animate={{ bottom: 15 }}
            exit={{ bottom: 0, opacity: 0 }}
        >
            <div className="Menu">
                {people.map((elem, i) => (
                    <MiniGrid
                        key={elem}
                        board={boards[elem]}
                        index={i}
                        selected={selected == elem}
                        playing={playing == elem}
                        self={self == elem}
                        value={elem}
                        onClick={() => onClick(elem)}
                        began={began}
                    />
                ))}
            </div>
            {showLeaderBoard && (
                <h1 className="TurnMenuItem Leaderboard" onClick={showLeaderBoard}>
                    <ProfileOutlined />
                </h1>
            )}
        </motion.div>
    );
};
