import React from 'react';
import { Button } from 'antd';

export const Waiting = ({ data, startGame }) => {
    const hasData = data && data['players'];
    const isLeader = hasData && data['leader'] == data['self'];
    const hasEnough = hasData && data['players'].length > 1;
    const hasGood = hasData && data['players'].length > 4;
    return (
        <div
            style={{
                position: 'absolute',
                left: '50%',
                top: '50%',
                transform: 'translate(-50%, -50%)',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
        >
            <h1 style={{ marginBottom: 30 }}>
                {!hasEnough && 'On your ones mate.'}
                {hasData && hasGood && 'Turn out!'}
                {hasData && hasEnough && 'Surely more for a leaderboard game ...'}
            </h1>
            {isLeader && hasEnough && (
                <Button type="primary" onClick={startGame}>
                    Start Game
                </Button>
            )}
        </div>
    );
};
