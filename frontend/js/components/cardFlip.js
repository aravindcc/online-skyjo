import * as React from 'react';
import { useEffect, useState } from 'react';

const CardFlip = React.forwardRef((props, ref) => {
    const {
        cardStyles: { back, front },
        cardZIndex,
        containerStyle,
        flipDirection,
        flipSpeedFrontToBack,
        flipSpeedBackToFront,
        infinite,
        shouldAnimate,
        onComplete,
    } = props;

    const [isFlipped, setFlipped] = useState(props.isFlipped);
    const [rotation, setRotation] = useState(0);

    useEffect(() => {
        if (props.isFlipped !== isFlipped) {
            setFlipped(props.isFlipped);
            setRotation((c) => c + 180);
            if (onComplete) {
                setTimeout(
                    (() => {
                        onComplete && onComplete();
                    }).bind(onComplete),
                    flipSpeedBackToFront * 1000 + 100
                );
            }
        }
    }, [props.isFlipped, onComplete]);

    const getComponent = (key) => {
        if (props.children.length !== 2) {
            throw new Error('Component CardFlip requires 2 children to function');
        }
        return props.children[key];
    };

    const frontRotateY = `rotateY(${infinite ? rotation : isFlipped ? 180 : 0}deg)`;
    const backRotateY = `rotateY(${infinite ? rotation + 180 : isFlipped ? 0 : -180}deg)`;
    const frontRotateX = `rotateX(${infinite ? rotation : isFlipped ? 180 : 0}deg)`;
    const backRotateX = `rotateX(${infinite ? rotation + 180 : isFlipped ? 0 : -180}deg)`;

    const animate = shouldAnimate();
    const styles = {
        back: {
            WebkitBackfaceVisibility: 'hidden',
            backfaceVisibility: 'hidden',
            height: '100%',
            left: '0',
            position: isFlipped ? 'relative' : 'absolute',
            top: '0',
            transform: flipDirection === 'horizontal' ? backRotateY : backRotateX,
            transformStyle: 'preserve-3d',
            transition: `${animate ? flipSpeedFrontToBack : 0}s`,
            width: '100%',
            ...back,
        },
        container: {
            perspective: '1000px',
            zIndex: `${cardZIndex}`,
            height: '100%',
        },
        flipper: {
            height: '100%',
            position: 'relative',
            width: '100%',
        },
        front: {
            WebkitBackfaceVisibility: 'hidden',
            backfaceVisibility: 'hidden',
            height: '100%',
            left: '0',
            position: isFlipped ? 'absolute' : 'relative',
            top: '0',
            transform: flipDirection === 'horizontal' ? frontRotateY : frontRotateX,
            transformStyle: 'preserve-3d',
            transition: `${animate ? flipSpeedBackToFront : 0}s`,
            width: '100%',
            zIndex: '2',
            ...front,
        },
    };

    return (
        <div
            className="react-card-flip"
            style={{ ...styles.container, ...containerStyle }}
            ref={ref}
        >
            <div className="react-card-flipper" style={styles.flipper}>
                <div className="react-card-front" style={styles.front}>
                    {getComponent(0)}
                </div>

                <div className="react-card-back" style={styles.back}>
                    {getComponent(1)}
                </div>
            </div>
        </div>
    );
});

CardFlip.defaultProps = {
    cardStyles: {
        back: {},
        front: {},
    },
    cardZIndex: 'auto',
    containerStyle: {},
    flipDirection: 'horizontal',
    flipSpeedBackToFront: 0.6,
    flipSpeedFrontToBack: 0.6,
    infinite: false,
    isFlipped: false,
    shouldAnimate: () => true,
};

export default CardFlip;
