export const DropZones = {
    FACEUP: 'FaceUp',
    FACEDOWN: 'FaceDown',
    TOUCH: 'Touch',
};

export const FOLDED_NUM = -3;
export const getColor = (val) => {
    return `static/card_images/${val == FOLDED_NUM ? 'back' : val}.jpeg`;
};

export const getRawColor = (val) => {
    switch (val) {
        case FOLDED_NUM:
            return 'black'; //black
        case -2:
            return 'DarkViolet'; // pruple
        case -1:
            return 'Purple'; // purple
        case 0:
            return 'blue'; // blue
        case 1:
            return '#00ff00'; // green
        case 2:
            return '#00b300'; // green
        case 3:
            return '#009900'; // green
        case 4:
            return '#008000'; // green
        case 5:
            return '#ffff40'; // yellow
        case 6:
            return '#ffff00'; // yellow
        case 7:
            return '#e6e600'; // yellow
        case 8:
            return '#cccc00'; //yellow
        case 9:
            return '#ff1a1a'; //red
        case 10:
            return '#ff0000'; //red
        case 11:
            return '#e60000'; //red
        case 12:
            return '#cc0000'; //red

        default:
            return 'red';
    }
};
