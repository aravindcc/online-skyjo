import { FOLDED_NUM } from './constants';

export function ogData() {
    var og = {};
    for (var i = 0; i < 4; i++) {
        og[i.toString()] = {};
        for (var j = 0; j < 3; j++) {
            og[i.toString()][j.toString()] = FOLDED_NUM;
        }
    }
    return og;
}

export function ogLookupPos() {
    var og = {};
    for (var i = 0; i < 4; i++) {
        og[i.toString()] = {};
        for (var j = 0; j < 3; j++) {
            og[i.toString()][j.toString()] = { x: 0, y: 0 };
        }
    }
    return og;
}

export const postData = (url = '', data = {}, altMethod = undefined) => {
    return fetch(url, {
        method: altMethod || 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': CSRFTOKEN,
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
};

export const Action = {
    BEGIN_GAME: 'BEGIN_GAME',
    PLAY_GIVE: 'PLAY_GIVE',
    PLAY_TAKE: 'PLAY_TAKE',
    PLAY_REJECT: 'PLAY_REJECT',
    OPEN_CARD: 'OPEN_CARD',
    ACK: 'ACK',
    ROUND: 'ROUND_ACK',
};
