import React, { useCallback, useEffect } from 'react';
import { useQuery } from 'react-query';
import Loading from '../components/Loader';
import CentralAlert from '../components/Alert';
import Leaderboard from '../components/Leaderboard';

import './style.scss';
import { message } from 'antd';
import { postData } from '../data';
import Lobby from './Lobby';

const Boot = () => {
    const { isLoading, error, data, refetch } = useQuery('leader', () =>
        fetch('/leader/').then((res) => {
            if (res.status == 200) {
                return res.json();
            } else {
                throw new Error('Invalid Request!');
            }
        })
    );

    const requestLeader = useCallback(() => {
        postData('/leader/', {}).then(() => {
            message.info({
                content: 'Joined Lobby!',
                style: { textAlign: 'left', marginLeft: 30 },
            });
            refetch();
        });
    }, [refetch]);

    if (isLoading) return <Loading style={{ height: '100%', marginTop: 100 }} fontSize="3rem" />;

    if (error)
        return (
            <CentralAlert
                style={{ height: '100%', marginTop: 100 }}
                message="who are you? 😠"
                type="error"
            />
        );

    return data == 'OK!' ? (
        <Lobby
            exitGame={() => {
                refetch();
                message.destroy('game_end');
            }}
        />
    ) : (
        <Leaderboard requestLeader={requestLeader} />
    );
};

export default Boot;
