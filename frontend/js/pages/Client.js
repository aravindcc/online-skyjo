import React, { useCallback, useEffect, useState } from 'react';
import { TurnBar } from '../components/TurnBar';
import { motion, AnimatePresence } from 'framer-motion';

import { Button, message, Modal } from 'antd';
import Game from './Game';
import { Waiting } from '../components/Waiting';
import { onResize } from '../components/BoardCard';
import Leaderboard from '../components/Leaderboard';
import { Action } from '../data';

export const Client = ({
    data,
    statusMessage,
    startGame,
    sendAction,
    transitionDirection,
    onEnd,
}) => {
    const [visibleLeaderboard, setVisibleLeaderboard] = useState(false);
    const [visibleTurns, setVisibleTurns] = useState(true);
    const [currentPlayer, setCurrentPlayer] = useState();
    const [waitingAck, setWaitingAck] = useState(false);
    // const onPan = useCallback(
    //     (event, { point, offset, velocity }) => {
    //         if (point.y < document.documentElement.clientHeight * 0.7) return;
    //         const dir = offset.y < 0 ? 'up' : 'down';

    //         if (dir === 'up' && velocity.y < -10) {
    //             // show
    //             setVisibleTurns(true);
    //         } else if (dir === 'down' && velocity.y > 10) {
    //             // don't show
    //             setVisibleTurns(false);
    //         }
    //     },
    //     [setVisibleTurns]
    // );
    const [clientHeight, setClientHeight] = useState(0);
    const resizeTable = useCallback(() => {
        const height = Math.min(window.screen.height, document.documentElement.clientHeight);
        const h = height * 0.98;
        setClientHeight(h);
    }, [setClientHeight]);

    const goNextRound = useCallback(() => {
        message.loading({
            content: 'Waiting for everyone to start round..',
            duration: 0,
            key: 'round_end',
        });
        sendAction(Action.ROUND);
    }, [sendAction]);

    const sendActionAckd = useCallback(
        (action, ack, i, j) => {
            sendAction(action, i, j);
            if (ack) {
                setWaitingAck(true);
            }
        },
        [sendAction]
    );

    const began = data && data['began'];
    const players = data['players'] || [];
    const boards = data['boards'] || {};
    const self = data['self'];
    const round = data['round'] || 0;
    const roundEnded = !!data['roundEnded'];
    const gameEnded = !!data['ended'];
    const currentGo = data['player'] || self;
    const shouldGo = currentPlayer == self && currentGo == self;

    const currentBoard = boards[currentPlayer] || {};
    const locked = began && (waitingAck || !shouldGo);

    const [currentRound, setCurrentRound] = useState(0);
    useEffect(() => setCurrentRound(round), [round]);
    onResize(resizeTable);
    useEffect(resizeTable, []);

    useEffect(() => {
        if (statusMessage == undefined) return;
        const ack = !!statusMessage['ack'];
        if (ack) {
            const name = statusMessage['name'];
            const player = statusMessage['player'];
            setWaitingAck(false);
            if (name) {
                const plural = name.slice(name.length - 1) === 's' ? "'" : "'s";
                message.info({
                    content: `It's ${name}'${plural} turn.`,
                    style: { textAlign: 'left', marginLeft: 30 },
                });
            }
            if (self == player) setCurrentPlayer(player);
        }
    }, [statusMessage]);

    useEffect(() => {
        const self = data['self'];
        if (!self) return;
        if (currentPlayer == undefined) setCurrentPlayer(self);
        if (data['round'] != currentRound) setWaitingAck(false);
        if (!data['began']) message.destroy('round_end');
    }, [data]);

    useEffect(() => {
        if (!roundEnded) {
            setVisibleLeaderboard(false);
            message.destroy('round_end');
            setCurrentPlayer(self);
        } else setVisibleLeaderboard(true);
    }, [roundEnded]);

    useEffect(() => {
        if (gameEnded) onEnd();
    }, [gameEnded]);

    return (
        <motion.div
            style={{
                position: 'absolute',
                width: '100vw',
                height: clientHeight,
                touchAction: 'none',
            }}
        >
            {began ? (
                <Game
                    board={currentBoard}
                    sendAction={sendActionAckd}
                    locked={locked}
                    transitionDirection={transitionDirection}
                    currentPlayer={currentPlayer}
                    playCard={data['play_card']}
                    prevPlay={data['prev_play']}
                    deckLocked={!!data['deck_locked']}
                    self={self}
                    visibleTurns={visibleTurns}
                    round={round}
                />
            ) : (
                <Waiting data={data} startGame={startGame} />
            )}
            <AnimatePresence>
                {visibleTurns && (
                    <TurnBar
                        selected={currentPlayer}
                        playing={currentGo}
                        data={players}
                        onClick={setCurrentPlayer}
                        showLeaderBoard={() => setVisibleLeaderboard(true)}
                        self={self}
                        boards={boards}
                        began={began}
                    />
                )}
            </AnimatePresence>
            <Modal
                title="Scores on the doors"
                visible={visibleLeaderboard}
                onOk={goNextRound}
                onCancel={() => setVisibleLeaderboard(false)}
                style={{ top: 20, padding: 0 }}
                className={'ScoreBoard'}
                cancelText="close"
                okText="Next round?"
                footer={
                    !roundEnded || gameEnded
                        ? [
                              <Button
                                  key="submit"
                                  type="primary"
                                  onClick={() => setVisibleLeaderboard(false)}
                              >
                                  ok
                              </Button>,
                          ]
                        : undefined
                }
            >
                <Leaderboard
                    inputLocation="score"
                    round={round}
                    marginTable={0}
                    tableWidth="100%"
                />
            </Modal>
        </motion.div>
    );
};
