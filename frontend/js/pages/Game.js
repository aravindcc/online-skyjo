import React, { useCallback, useEffect, useRef, useState } from 'react';
import { DndProvider } from 'react-dnd';
import MultiBackend from 'react-dnd-multi-backend';
import HTML5toTouch from 'react-dnd-multi-backend/dist/esm/HTML5toTouch';
import { AnimatedCard } from '../components/AnimatedCard';
import { AnimatedRow } from '../components/AnimatedRow';
import { Bins } from '../components/Bins';
import { onResize } from '../components/BoardCard';
import { DropZones, FOLDED_NUM } from '../constants';
import { ogData, ogLookupPos, Action } from '../data';

var keepAnimHonest = 0;
const lookupPos = ogLookupPos();
const Game = ({
    locked,
    sendAction,
    board,
    playCard,
    prevPlay,
    deckLocked,
    transitionDirection,
    currentPlayer,
    self,
    visibleTurns,
}) => {
    const [data, setData] = useState(ogData());
    const [openPileVal, setOpenPileVal] = useState(5);
    const [deckVal, setDeckVal] = useState(FOLDED_NUM);
    const [animObject, setAnimObject] = useState({});
    const container = useRef();
    const [CARD_HEIGHT, setCardHeight] = useState(0);
    const [CARD_WIDTH, setCardWidth] = useState(0);
    const [BOARD_HEIGHT, setBoardHeight] = useState(0);

    const resizeCards = useCallback(() => {
        if (container.current) {
            const { width, height } = container.current.getBoundingClientRect();
            const h = Math.floor((height - 40 - 30 * 4) / 5);
            setCardHeight(h);
            setCardWidth((h * 1183) / 1917);
            setBoardHeight(`calc(${height}px - ${h}px - 40px)`);
        }
    }, [container, setCardHeight, setCardWidth]);

    onResize(resizeCards);
    useEffect(resizeCards, [container.current]);

    const doReverseAnim = useCallback(
        (monitor, val) => {
            if (!lookupPos[DropZones.FACEUP]) return;
            const sou = monitor.getSourceClientOffset();
            const des = lookupPos[DropZones.FACEUP];
            setAnimObject({
                startPoint: sou,
                endPoint: des,
                value: val,
            });
        },
        [setAnimObject, lookupPos]
    );

    const updateCell = useCallback(
        (v, i, j, type) => {
            if (v == undefined) {
                if (deckLocked || openPileVal == FOLDED_NUM)
                    sendAction(Action.OPEN_CARD, false, i, j);
                return;
            }
            const n = { ...data };
            n[i][j] = v;
            setData(n);
            sendAction(Action.PLAY_TAKE, true, i, j);
        },
        [data, setData, sendAction, deckLocked, open]
    );

    const drawDeck = useCallback(() => sendAction(Action.PLAY_GIVE, false), [sendAction]);

    const checkVisible = useCallback((row, i) => {
        if (row[0] == FOLDED_NUM) return true;

        var allsame = true;
        var chosen = row[0];
        Object.values(row).forEach((k) => {
            if (k != chosen) {
                allsame = allsame && false;
            }
        });
        return !allsame;
    }, []);

    useEffect(() => {
        if (board) {
            for (var i = 0; i < 4; i++) {
                const row = i.toString();
                if (!(row in board) && row in data) {
                    var num = FOLDED_NUM;
                    board[row] = {};
                    for (var j = 0; j < 3; j++) {
                        const col = j.toString();
                        if (data[row][col] != FOLDED_NUM) {
                            num = data[row][col];
                            break;
                        }
                    }
                    if (num == FOLDED_NUM || num == undefined) continue;
                    for (var j = 0; j < 3; j++) {
                        const col = j.toString();
                        board[row][col] = num;
                    }
                }
            }
            setData(board);
        }
    }, [board]);

    useEffect(() => {
        if (playCard != undefined) {
            if (deckLocked) {
                setDeckVal(playCard);
                setOpenPileVal(prevPlay == undefined ? FOLDED_NUM : prevPlay);
            } else {
                setDeckVal(FOLDED_NUM);
                setOpenPileVal(playCard);
            }
        } else {
            setDeckVal(FOLDED_NUM);
            setOpenPileVal(FOLDED_NUM);
        }
    }, [playCard, board]);

    useEffect(() => {
        if (transitionDirection) {
            if (transitionDirection['origin'] != self) {
                // update the open pile and open deck
                setDeckVal(FOLDED_NUM);
                setOpenPileVal(transitionDirection['last_play']);

                const onPlayer = transitionDirection['origin'] == currentPlayer;
                const sou = transitionDirection['deck_locked']
                    ? lookupPos[DropZones.FACEDOWN]
                    : lookupPos[DropZones.FACEUP];
                const des = onPlayer
                    ? lookupPos[transitionDirection['row']][transitionDirection['col']]
                    : sou;
                const val = transitionDirection['prev_play'];
                const play = transitionDirection['play_card'];
                setAnimObject({
                    startPoint: sou,
                    endPoint: des,
                    value: val,
                    reverseObj: {
                        startPoint: onPlayer ? des : lookupPos[DropZones.FACEUP],
                        endPoint: lookupPos[DropZones.FACEUP],
                        value: play,
                        onAnimationComplete: () => {},
                    },
                    count: keepAnimHonest++,
                });
            } else {
                // sendAction(Action.ACK);
            }
        }
    }, [transitionDirection]);

    return (
        <>
            <DndProvider backend={MultiBackend} options={HTML5toTouch}>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        left: '50%',
                        position: 'absolute',
                        transform: 'translate(-50%, 0)',
                        width: '100vw',
                        maxWidth: '800px',
                        height: '100%',
                        marginTop: 20,
                        pointerEvents: locked ? 'none' : 'inherit',
                    }}
                    ref={container}
                >
                    <Bins
                        {...{
                            openPileVal,
                            drawDeck,
                            deckVal,
                            lookupPos,
                            CARD_WIDTH,
                            CARD_HEIGHT,
                        }}
                    />
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            width: '95%',
                            height: visibleTurns
                                ? `calc(${BOARD_HEIGHT} - 25px - 5rem)`
                                : BOARD_HEIGHT,
                            marginTop: 10,
                            marginBottom: 30,
                            transition: 'height 0.5s',
                        }}
                    >
                        {Object.entries(data).map(([i, row]) => (
                            <AnimatedRow
                                {...{
                                    data,
                                    i,
                                    row,
                                    checkVisible,
                                    updateCell,
                                    doReverseAnim,
                                    lookupPos,
                                    CARD_HEIGHT,
                                    CARD_WIDTH,
                                }}
                                key={i}
                            />
                        ))}
                    </div>
                </div>
            </DndProvider>
            <AnimatedCard {...animObject} {...{ setOpenPileVal, CARD_HEIGHT, CARD_WIDTH }} />
        </>
    );
};

export default Game;
