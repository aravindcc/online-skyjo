import React, { useCallback, useEffect, useMemo, useState } from 'react';
import useWebSocket from 'react-use-websocket';

import './style.scss';
import { Action } from '../data';
import { Client } from './Client';
import { message } from 'antd';

const ws_scheme = window.location.protocol == 'https:' ? 'wss' : 'ws';
const socketUrl = `${ws_scheme}://${location.host}/ws/game/`;

var reAckTimer;
var sequence = 0;
const Lobby = ({ exitGame }) => {
    const { sendJsonMessage, lastMessage } = useWebSocket(socketUrl);
    const inputMessage = useMemo(() => {
        if (lastMessage) {
            return JSON.parse(lastMessage.data);
        }
        return {};
    }, [lastMessage]);
    const [data, setData] = useState({});
    const [statusMessage, setStatusMessage] = useState();
    const [transitionDirection, setTransitionDirection] = useState();

    const startGame = useCallback(() => {
        sendJsonMessage({
            action: Action.BEGIN_GAME,
        });
    }, [sendJsonMessage]);

    const sendAction = useCallback(
        (action, i, j) => {
            if (action == Action.ROUND) {
                reAckTimer = setInterval(
                    (() => {
                        sendJsonMessage({ action });
                    }).bind(sendJsonMessage),
                    Math.floor(Math.random() * 50) + 20
                );
            }
            sendJsonMessage({
                action,
                row: i,
                col: j,
            });
        },
        [sendJsonMessage]
    );

    const onEnd = useCallback(() => {
        message.info({
            content: 'Game ended: click to view results.',
            duration: 0,
            style: { textAlign: 'left', marginLeft: 30 },
            key: 'game_end',
            onClick: (() => {
                exitGame && exitGame();
                message.destroy('game_end');
            }).bind(exitGame),
        });
    }, [exitGame]);

    useEffect(() => {
        if (inputMessage['time'] == true) {
            sendJsonMessage({
                stamp: inputMessage['stamp'],
            });
            return;
        }
        if (inputMessage['sequence'] <= sequence) {
            return;
        }
        sendJsonMessage({ action: Action.ACK });
        sequence = inputMessage['sequence'];
        if (reAckTimer) {
            clearInterval(reAckTimer);
            reAckTimer = undefined;
        }
        if (inputMessage['ack'] == true) {
            const { data, ...status } = inputMessage;
            data['self'] = status['self'];
            setStatusMessage(status);
            setData(data);
        } else if (inputMessage['game_end'] == true) {
            setData(inputMessage.data);
        } else if (inputMessage['transistion']) {
            setTransitionDirection({
                ...inputMessage['transistion'],
                origin: inputMessage['player'],
            });
        } else setData(inputMessage);
    }, [inputMessage]);

    return (
        <Client
            data={data}
            onEnd={onEnd}
            statusMessage={statusMessage}
            sendAction={sendAction}
            startGame={startGame}
            transitionDirection={transitionDirection}
            onEnd={onEnd}
        />
    );
};

export default Lobby;
