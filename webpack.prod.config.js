const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleTracker = require('webpack-bundle-tracker');
const path = require('path');

const baseConfig = require('./webpack.base.config');

const nodeModulesDir = path.resolve(__dirname, 'node_modules');

baseConfig.mode = 'production';
baseConfig.devtool = 'source-map';

baseConfig.entry = ['whatwg-fetch', '@babel/polyfill', './frontend/js/index.js'];

baseConfig.optimization = {
    splitChunks: {
        chunks: 'all',
        name: 'vendors~main',
    },
};

baseConfig.output = {
    path: path.resolve('./backend/js-build/webpack_bundles'),
    publicPath: '/static/',
    filename: '[name].js',
};

baseConfig.module.rules.push(
    {
        test: /\.jsx?$/,
        exclude: [nodeModulesDir],
        use: {
            loader: 'babel-loader',
            options: {
                presets: ['@babel/preset-env', '@babel/preset-react'],
            },
        },
    },
    {
        test: /\.(woff(2)?|eot|ttf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
            {
                loader: 'file-loader',
                options: { name: 'fonts/[name].[ext]' },
            },
        ],
    }
);

baseConfig.plugins = [
    new webpack.DefinePlugin({
        // removes React warnings
        'process.env': {
            NODE_ENV: JSON.stringify('production'),
        },
    }),
    new MiniCssExtractPlugin({ filename: '[name]-[hash].css' }),
    new BundleTracker({
        filename: '../../webpack-stats.json',
    }),
    new webpack.LoaderOptionsPlugin({
        options: {
            context: __dirname,
            postcss: [autoprefixer],
        },
    }),
];

module.exports = baseConfig;
